.. index documentation master file, created by
   sphinx-quickstart on Thu Mar 14 14:59:47 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Login and Register
==============================================

==========================
I.Login
==========================

1. Thêm quyền vào file Android manifest:

** Thêm quyền truy cập::

	<uses-permission android:name="android.permission.INTERNET"></uses-permission>
	
** Thêm activity vào file::

    <activity
     android:name="com.vn.admin.gametvsdklib.WebViewResponseActivity"
     android:screenOrientation="portrait" />	
		
		
2. Thêm thư viện vào file build.grape::

    implementation 'com.google.firebase:firebase-messaging:17.4.0'
    implementation 'com.google.code.gson:gson:2.6.2'
    implementation 'com.squareup.retrofit2:converter-gson:2.0.2'
    implementation 'com.squareup.retrofit2:retrofit:2.0.2'
	
	
		
3. Vào Activity của bạn thêm đoạn code sau:

** Bạn khởi tạo 1 button để click gọi sang activity của chúng tôi.Ví dụ như::

 Button btnLogin = (Button) findViewById(R.id.btn_login);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(MainActivity.this, WebViewResponseActivity.class);
                mIntent.putExtra(Constant.KEY_CONNECT, Constant.KEY_LOGIN);
                startActivityForResult(mIntent, REQUEST_CODE);
            }
        });
		
** Nó sẽ gọi sang trang Web bên chúng tôi để tương tác.Nếu login thành công sẽ trả về 1 user_hask là 1 chuỗi String 
trong hàm Activity Result::

  @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CODE && data != null) {
                String strMessage = data.getStringExtra(Constant.KEY_HASK);
                Log.e(TAG, strMessage);

            }
        }

    }
	
Bạn có thể lưu lại String để phục vụ việc lưu vào xóa user_hask này.Chúng tôi nghĩ là bạn nên lưu vào Sharepreferen để tiện dùng.Bạn vào file Application
của mình khai báo như sau. Ví dụ::

    public class YourApplication extends Application {

    private static YourApplication mInstance;

    public static YourApplication get() {
        return mInstance;
    }

    private static SharedPreferences PREF;
    
    @Override
    public void onCreate() {
        super.onCreate();

        mInstance = this;
        WindowManager wm = (WindowManager) getSystemService(WINDOW_SERVICE);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        assert wm != null;
        wm.getDefaultDisplay().getMetrics(displayMetrics);

        if (PREF == null)
            PREF = getSharedPreferences(Constant.F2LD, Context.MODE_PRIVATE);
    }

    public static void saveUserHash(String profilePicture) {
        SharedPreferences.Editor editor = PREF.edit();
        editor.putString(Constant.KEY_USER_HASH, profilePicture);
        editor.apply();
    }

    public static String getUserHash() {
        return PREF.getString(Constant.KEY_USER_HASH, "");
    }

    public static void clearUserHashSave(Context context) {
        SharedPreferences.Editor editor = PREF.edit();
        editor.remove(Constant.KEY_USER_HASH);
        editor.apply();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            CookieManager.getInstance().removeAllCookies(null);
            CookieManager.getInstance().flush();
        } else {
            CookieSyncManager cookieSyncMngr = CookieSyncManager.createInstance(context);
            cookieSyncMngr.startSync();
            CookieManager cookieManager = CookieManager.getInstance();
            cookieManager.removeAllCookie();
            cookieManager.removeSessionCookie();
            cookieSyncMngr.stopSync();
            cookieSyncMngr.sync();
        }

    }
   }
  


===============================
II.Register And FastRegister
===============================

2. Vào Activity của bạn thêm đoạn code sau:

** Bạn khởi tạo 1 button để click gọi sang activity của chúng tôi.Ví dụ như::

 Button btnLogin = (Button) findViewById(R.id.btn_login);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(MainActivity.this, WebViewResponseActivity.class);
                mIntent.putExtra(Constant.KEY_CONNECT, Constant.KEY_LOGIN);
                startActivityForResult(mIntent, REQUEST_CODE);
            }
        });
   
Thay Constant.KEY_LOGIN bằng Constant.KEY_REGISTER;Constant.KEY_REGISTER_FAST.

** Nó sẽ gọi sang trang Web bên chúng tôi để tương tác.Nếu login thành công sẽ trả về 1 user_hask là 1 chuỗi String 
trong hàm Activity Result::

  @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CODE && data != null) {
                String strMessage = data.getStringExtra(Constant.KEY_HASK);
                Log.e(TAG, strMessage);

            }
        }

    }
	
Bạn có thể lưu lại String để phục vụ việc lưu vào xóa user_hask này.
	


