.. index documentation master file, created by
   sphinx-quickstart on Thu Mar 14 14:59:47 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

FacebookToken 
==============================================

1. Vào Activity của bạn thêm đoạn code sau:

** Bạn khởi tạo 1 button để click gọi sang activity của chúng tôi.Ví dụ như::

 Button btnLogin = (Button) findViewById(R.id.btn_login);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(MainActivity.this, WebViewResponseActivity.class);
                mIntent.putExtra(Constant.KEY_CONNECT, Constant.KEY_LOGIN);
                startActivityForResult(mIntent, REQUEST_CODE);
            }
        });
		
** Nó sẽ gọi sang trang Web bên chúng tôi để tương tác.Nếu login thành công sẽ trả về 1 user_hask là 1 chuỗi String 
trong hàm Activity Result::

  @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CODE && data != null) {
                String strMessage = data.getStringExtra(Constant.KEY_HASK);
                Log.e(TAG, strMessage);

            }
        }

    }
	
Bạn có thể lưu lại String để phục vụ việc lưu vào xóa user_hask này.
