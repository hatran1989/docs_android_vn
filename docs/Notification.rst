.. index documentation master file, created by
   sphinx-quickstart on Thu Mar 14 14:59:47 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Notification
==============================================

==========================
I.Notification
==========================

1. Thêm quyền vào file Android manifest:

** Thêm quyền truy cập::

	<uses-permission android:name="android.permission.INTERNET"></uses-permission>
	
** Thêm activity vào file::

    <activity
     android:name="com.vn.admin.gametvsdklib.WebViewResponseActivity"
     android:screenOrientation="portrait" />	
		
		
2. Thêm thư viện vào file build.grape::

    implementation 'com.google.firebase:firebase-messaging:17.4.0'
    implementation 'com.google.code.gson:gson:2.6.2'
    implementation 'com.squareup.retrofit2:converter-gson:2.0.2'
    implementation 'com.squareup.retrofit2:retrofit:2.0.2'
	
	
	

	
