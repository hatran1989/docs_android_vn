.. index documentation master file, created by
   sphinx-quickstart on Thu Mar 14 14:59:47 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Gametv Sdk Android Documentation
==============================================
             GametvSdk_12345678
			 

.. toctree::
   :maxdepth: 2

   Overview
   LoginAndRegister
   FacebookToken
   Notification
   Payment

=========================
I.Download
=========================

**Link here:** 

==========================
II.Thông tin thiết lập
==========================
GametvSdk cung cấp cho bạn tất cả các thông tin cần thiết như:game Id,package name,Facebook App Id, danh sách các gói thanh toán, danh sách API,...tất cả được tìm thấy trong các tài liệu,được cung cấp bởi các vận hành game của chúng tôi.

Nếu bạn không hiểu bất cứ điều gì, xin vui lòng liên hệ với vận hành game của chúng tôi, dịch thuật hoặc bất cứ những ai bạn có thể liên lạc.

==========================
III.Thêm Sdk vào Project
==========================

1. Thêm file .aar vào thư mục libs.

.. image:: images/1.import_libs.PNG

2. Chọn File>> New>> Import Module

.. image:: images/2.import_libs.PNG

3. Chọn File>> ProjectS Structure
4. Chọn Module "app".
5. Chọn tabs "Dependencies" ấn dấu "+" chọn "3 Module Dependencies"

.. image:: images/3.import_libs.PNG

6. Chọn Modules để add

.. image:: images/4.import_libs.PNG

Lúc này trong file build.gradle sẽ add libs GametvSdk vào.

.. image:: images/5.import_libs.PNG


7. Thêm thư viện vào file build.grape::

    implementation 'com.google.firebase:firebase-messaging:17.4.0'
    implementation 'com.google.code.gson:gson:2.6.2'
    implementation 'com.squareup.retrofit2:converter-gson:2.0.2'
    implementation 'com.squareup.retrofit2:retrofit:2.0.2'
    implementation 'com.facebook.android:facebook-login:[4,5)'
    implementation 'com.squareup.okhttp3:logging-interceptor:3.9.0'
	
	
8. Thêm dòng code sau vào file "strings.xml" như sau ::

    <string name="google_app_id" translatable="false">1:922641855266:android:1dfc8a8f1cc57dd8</string>
	 <!--FaceBook app id-->
    <string name="facebook_app_id">2292283651024631</string>
    <string name="fb_login_protocol_scheme">fb2292283651024631</string>
	
Bạn đã hoàn tất việc add libs của chúng tôi vào project của bạn.




